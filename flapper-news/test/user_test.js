var expect = require('chai').expect;
var app = require('../app');
var request = require('supertest');

var agent = request.agent(app);
var User = require('../models/Users');

describe('POST /login', function() {
  it('user \'test\' should now be logged in', function(done) {
    agent.post('/login')
      .send({'username': 'test','password': 'test'
      })
      .end(function(err, res) {
        if (err) {
          return done(err);
        }
        expect(res.statusCode).equal(200);
        expect(res.text).be.json;

        var fetchedData = JSON.parse(res.text);

        expect(fetchedData).to.be.an('object');
        expect(fetchedData).to.not.empty;
        expect(fetchedData).to.have.property('token');
        done();
      });
  });
});

describe('POST /login', function() {
  it('user \'test\' should now be logged in', function(done) {
    agent.post('/login')
      .send({'username': 'notauser','password': 'notapassword'
      })
      .end(function(err, res) {
        if (err) {
          return done(err);
        }
        expect(res.statusCode).equal(401);
        done();
      });
  });
});

describe('POST /register', function(){
  it('user should be registered', function(done) {
    agent.post('/register')
    .send({'username': 'flapper-news2', 'password': 'flapper-news2'})
    .end(function(err, res){
      if(err) {return done(err);}
      expect(res.statusCode).equal(200);
      expect(res.text).be.json;

      var fetchedData = JSON.parse(res.text);

      expect(fetchedData).to.be.an('object');
      expect(fetchedData).to.not.empty;
      expect(fetchedData).to.have.property('token');
      done();
    });
  });
});
