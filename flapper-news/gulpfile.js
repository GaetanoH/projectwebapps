var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var sass = require('gulp-sass');
var gulp = require('gulp');
var jshint = require('gulp-jshint');
var sourcemaps = require('gulp-sourcemaps');
var plumber = require('gulp-plumber');
var	liveReload = require('gulp-livereload');

var plugins = gulpLoadPlugins();
var forProduction = false;

// define the default task and add the watch task to it
gulp.task('default', ['watch']);

// configure the jshint task
gulp.task('jshint', function() {
  return gulp.src('public/javascripts/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

/*Runs all tests, in the testfolder*/
gulp.task('runTests', function() {
  return gulp.src('./test/*.js')
    .pipe(plugins.mocha());
});

/*SCSS to CSS compiling*/
gulp.task('styles', function() {
    gulp.src('public/stylesheets/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('public/stylesheets/'));
});

/*Makes the js files production ready when forProd is true*/
gulp.task('build-js', function() {
  return gulp.src('public/javascripts/*.js')
    .pipe(sourcemaps.init())
      .pipe(concat('bundle.js'))
      //only uglify if gulp is ran with '--type production'
      .pipe(gutil.env.type === forProd ? uglify() : gutil.noop())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/assets/javascript'));
});

/*Makes css files production ready*/
gulp.task('build-css', function() {
  return gulp.src('source/scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('public/assets/stylesheets'));
});

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
  gulp.watch('public/stylesheets/style.scss', ['styles']);
  gulp.watch('public/javascripts/*.js', ['jshint']);
    gulp.watch(['./public/**']).on('change', function(file) {
        liveReload().changed(file.path);
    })
});
